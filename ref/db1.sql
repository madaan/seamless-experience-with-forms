-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 08, 2012 at 03:55 PM
-- Server version: 5.5.20
-- PHP Version: 5.3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*
--
--use `sefa`
--


-- --------------------------------------------------------

--
-- Table structure for table `designation`
--
*/
CREATE TABLE IF NOT EXISTS `designation` (
  `desig_id` varchar(20) NOT NULL,	
  `post` varchar(10) NOT NULL,
  `description` integer (40) NOT NULL,
  `ministry` varchar(10) NOT NULL,
  `organization` varchar(20) NOT NULL,
  `department` varchar(10) NOT NULL,
  `section` varchar(10) NOT NULL,
  PRIMARY KEY (`desig_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*
-- --------------------------------------------------------

--
-- Table structure for table `employee`
--
*/

CREATE TABLE IF NOT EXISTS `employee` (
  `e_id` integer (20) NOT NULL,
  `emp_Name` varchar( 20) NOT NULL,
  `desig_id` integer (11) NOT NULL,
  `pay_id` integer (11) NOT NULL,
  `service` varchar(20) NOT NULL,
  `cadre` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `date_joining` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `marital_status` varchar(10) NOT NULL,
	`reporting_eid` int(20) NOT NULL, /*all of their eid should be stored*/
	`reviewing_eid` int(20) NOT NULL, /*--this eliminates the need of storing their names separately */
	`admin_auth` int(20) NOT NULL  ,
  PRIMARY KEY (`e_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*
-- --------------------------------------------------------
--
-- Table structure for table `login`
--*/
CREATE TABLE IF NOT EXISTS `login` (
  `e_id` int(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `lastAccess` varchar(10) NOT NULL,
  PRIMARY KEY (`e_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*
-- --------------------------------------------------------

--
-- Table structure for table `message`
--
*/
CREATE TABLE IF NOT EXISTS `message` (
  `msg_id` int(11) NOT NULL AUTO_INCREMENT,
  `to_id` int(20) NOT NULL, /* --should be of exactly the same type as the employee id because this is the employee id*/
  `role_sender` varchar(10) NOT NULL,
  `from_id` int(20) NOT NULL,
  `role_receiver` varchar(10) NOT NULL,
  `Comment` varchar(40) NOT NULL,
  `form_id` int(20) NOT NULL,  /*--actually e_id of the person whose form is under consideration*/
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`msg_id`)
)
 ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
/*
-- --------------------------------------------------------

--
-- Table structure for table `payscale`
--*/
CREATE TABLE IF NOT EXISTS `payscale` (
  `payGroupID` int(11) NOT NULL,
  `payGroupName` varchar(10) NOT NULL,
  `minPay` int(11) NOT NULL,
  `maxPay` int(11) NOT NULL,
  PRIMARY KEY (`payGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*
--
-- Dumping data for table `payscale`
--

INSERT INTO `payscale` (`payGroupID`, `payGroupName`, `minPay`, `maxPay`) VALUES
(1, 'payGrp1', 10000, 20000);
-- --------------------------------------------------------
--
-- Table structure for table `empExtra`
--
--many fields like the leave has to be made null
--it is not compulsory that someone has taken all the leaves that are available
*/


CREATE TABLE IF NOT EXISTS `empExtra` (
  `e_id` int(20) NOT NULL,
  `start_date` date ,
  `end_date` date ,
	`extension` int(4) DEFAULT 0, /*--initial extension*/ 
  `reporting_officer_name` varchar(20) NOT NULL,
  `year_allotment` int(11) NOT NULL,
  `present_grade` varchar(20) NOT NULL,
  `present_post` varchar(20) NOT NULL,
  `allotment_date` date NOT NULL,
  `period_reporting` int(11) NOT NULL,
  `period_reviewing` int(11) NOT NULL,
  `period_accepting` int(11) NOT NULL,
  /* --following data may very well be null*/
  `leave_period` int(11)  ,
  `leave_type` varchar(20)  ,
  `leave_remarks` varchar(50)  ,
  `other_leave_period` int(11)  ,
  `other_leave_type` varchar(20)  ,
  `other_leave_remarks` varchar(50)  ,
  `training1_start` date  ,
  `training1_end` date  ,
  `training1_institute` varchar(20)  ,
  `training1_subject` varchar(20)  ,
  `training2_start` date  ,
  `training2_end` date  ,
  `training2_instite` varchar(20)  ,
  `training2_subject` varchar(20)  ,
  `training3_start` date  ,
  `training3_end` date  ,
  `training3_institute` varchar(20)  ,
  `training3_subject` varchar(20)  ,
  `award1` varchar(100)  ,
  `award2` varchar(100)  ,
  `par` varchar(100) NOT NULL,
  `date_Property_Return` date NOT NULL,
  `date_Medical` date NOT NULL,
  `medical_Report` varchar(100) NOT NULL,
  `date_sec1` date NOT NULL,
  `sign_sec1` varchar(10) NOT NULL,
  PRIMARY KEY (`e_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;