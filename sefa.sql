-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 24, 2012 at 01:27 AM
-- Server version: 5.5.20
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sefa`
--

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE IF NOT EXISTS `designation` (
  `desig_id` varchar(20) NOT NULL,
  `post` varchar(10) NOT NULL,
  `description` int(40) NOT NULL,
  `ministry` varchar(10) NOT NULL,
  `organization` varchar(20) NOT NULL,
  `department` varchar(10) NOT NULL,
  `section` varchar(10) NOT NULL,
  PRIMARY KEY (`desig_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `e_id` int(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `emp_Name` varchar(20) NOT NULL,
  `desig_id` int(11) NOT NULL,
  `pay_id` int(11) NOT NULL,
  `service` varchar(20) NOT NULL,
  `cadre` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `date_joining` date NOT NULL,
  `gender` varchar(1) NOT NULL,
  `marital_status` varchar(10) NOT NULL,
  `reporting_eid` int(20) NOT NULL,
  `reviewing_eid` int(20) NOT NULL,
  `admin_auth` int(20) NOT NULL,
  PRIMARY KEY (`e_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`e_id`, `password`, `emp_Name`, `desig_id`, `pay_id`, `service`, `cadre`, `dob`, `date_joining`, `gender`, `marital_status`, `reporting_eid`, `reviewing_eid`, `admin_auth`) VALUES
(2, 'qwerty', 'sg', 1, 1, 'IAS', '2002', '0002-02-02', '0001-01-02', 'm', 'Unmarried', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `form`
--

CREATE TABLE IF NOT EXISTS `form` (
  `e_id` int(20) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `extension` int(4) DEFAULT '0',
  `reporting_officer_name` varchar(20) NOT NULL,
  `year_allotment` int(11) NOT NULL,
  `present_grade` varchar(20) NOT NULL,
  `present_post` varchar(20) NOT NULL,
  `allotment_date` date NOT NULL,
  `period_reporting` int(11) NOT NULL,
  `period_reviewing` int(11) NOT NULL,
  `period_accepting` int(11) NOT NULL,
  `leave_period` int(11) DEFAULT NULL,
  `leave_type` varchar(20) DEFAULT NULL,
  `leave_remarks` varchar(50) DEFAULT NULL,
  `other_leave_period` int(11) DEFAULT NULL,
  `other_leave_type` varchar(20) DEFAULT NULL,
  `other_leave_remarks` varchar(50) DEFAULT NULL,
  `training1_start` date DEFAULT NULL,
  `training1_end` date DEFAULT NULL,
  `training1_institute` varchar(20) DEFAULT NULL,
  `training1_subject` varchar(20) DEFAULT NULL,
  `training2_start` date DEFAULT NULL,
  `training2_end` date DEFAULT NULL,
  `training2_instite` varchar(20) DEFAULT NULL,
  `training2_subject` varchar(20) DEFAULT NULL,
  `training3_start` date DEFAULT NULL,
  `training3_end` date DEFAULT NULL,
  `training3_institute` varchar(20) DEFAULT NULL,
  `training3_subject` varchar(20) DEFAULT NULL,
  `award1` varchar(100) DEFAULT NULL,
  `award2` varchar(100) DEFAULT NULL,
  `par` varchar(100) NOT NULL,
  `date_Property_Return` date NOT NULL,
  `date_Medical` date NOT NULL,
  `medical_Report` varchar(100) NOT NULL,
  `date_sec1` date NOT NULL,
  `sign_sec1` varchar(10) NOT NULL,
  `duty_desc` varchar(200) DEFAULT NULL,
  `task1` varchar(100) DEFAULT NULL,
  `initialDeliv1` varchar(100) DEFAULT NULL,
  `midDeliv1` varchar(100) DEFAULT NULL,
  `finalDeliv1` varchar(100) DEFAULT NULL,
  `task2` varchar(100) DEFAULT NULL,
  `initialDeliv2` varchar(100) DEFAULT NULL,
  `midDeliv2` varchar(100) DEFAULT NULL,
  `finalDeliv2` varchar(100) DEFAULT NULL,
  `task3` varchar(100) DEFAULT NULL,
  `initialDeliv3` varchar(100) DEFAULT NULL,
  `midDeliv3` varchar(100) DEFAULT NULL,
  `finalDeliv` varchar(100) DEFAULT NULL,
  `exceptional_contribution` varchar(100) DEFAULT NULL,
  `hindering_factor` varchar(100) DEFAULT NULL,
  `skill_current` varchar(100) DEFAULT NULL,
  `filed_return` varchar(1) DEFAULT NULL COMMENT 'y/n ',
  `medical_checkup` varchar(1) DEFAULT NULL COMMENT 'y/n',
  `annual_plan_set` varchar(1) DEFAULT NULL COMMENT 'y/n',
  `sign_reporting` varchar(100) DEFAULT NULL,
  `data_section2` date DEFAULT NULL COMMENT 'Date at which section 2 was reviewed ',
  `agree_sec2` varchar(100) DEFAULT NULL,
  `comment_exceptional_claim` varchar(100) DEFAULT NULL,
  `significant_failure` varchar(100) DEFAULT NULL,
  `skill_demand_comment` varchar(100) DEFAULT NULL,
  `rev_work_output` int(2) DEFAULT NULL,
  `rep_work_output` int(2) DEFAULT NULL,
  `rev_personal_attrib` int(2) DEFAULT NULL,
  `rep_functional_competency` int(2) DEFAULT NULL,
  `rev_functional_competency` int(2) DEFAULT NULL,
  `comment_integrity` varchar(100) DEFAULT NULL,
  `rep_pen_picture` varchar(100) DEFAULT NULL,
  `domain_suggestions` varchar(100) DEFAULT NULL COMMENT 'Upto 4 suggestions can be given,they will be stored in the table in csv format',
  `overall_section3` int(2) DEFAULT NULL,
  `date_section3` date DEFAULT NULL,
  `sign_rep` varchar(100) DEFAULT NULL,
  `agree_sec3` varchar(1) DEFAULT NULL,
  `diff_opinion` varchar(100) DEFAULT NULL,
  `comment_pen_picture` varchar(100) DEFAULT NULL,
  `review_recommendations` varchar(100) NOT NULL COMMENT 'Again 4 options in csv format',
  `overall_grade4` int(2) DEFAULT NULL,
  `date_section4` date DEFAULT NULL,
  `sign_rev` varchar(100) DEFAULT NULL,
  `accepting_agree` varchar(1) DEFAULT NULL,
  `accepting_diff_opinion` varchar(100) DEFAULT NULL,
  `accepting_overall_grade` int(2) DEFAULT NULL,
  `date_section5` date DEFAULT NULL,
  `sign_accepting` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`e_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table to store forms. Each row represents one form.';

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `e_id` int(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `lastAccess` int(1) NOT NULL,
  PRIMARY KEY (`e_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`e_id`, `email`, `password`, `lastAccess`) VALUES
(2, 'sg@sg.com', 'qwerty', 1);

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `msg_id` int(11) NOT NULL AUTO_INCREMENT,
  `to_id` int(20) NOT NULL,
  `role_sender` varchar(10) NOT NULL,
  `from_id` int(20) NOT NULL,
  `role_receiver` varchar(10) NOT NULL,
  `Comment` varchar(40) NOT NULL,
  `form_id` int(20) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`msg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`msg_id`, `to_id`, `role_sender`, `from_id`, `role_receiver`, `Comment`, `form_id`, `time`) VALUES
(1, 2, 'Admin', 0, 'Appraised', 'Appraisal Process Begins', 2, '2012-07-20 13:15:34');

-- --------------------------------------------------------

--
-- Table structure for table `payscale`
--

CREATE TABLE IF NOT EXISTS `payscale` (
  `payGroupID` int(11) NOT NULL,
  `payGroupName` varchar(10) NOT NULL,
  `minPay` int(11) NOT NULL,
  `maxPay` int(11) NOT NULL,
  PRIMARY KEY (`payGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payscale`
--

INSERT INTO `payscale` (`payGroupID`, `payGroupName`, `minPay`, `maxPay`) VALUES
(1, 'payGrp1', 10000, 20000);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
