//sg
function getLocalData(category)
{

//if user has saved some information before
if(localStorage.saved)
{
	
if(localStorage.aprstart)
{
document.forms['f1s1']['aprStart'].value=localStorage.aprstart;
}
if(category==1)
{
	
document.forms['f1s1']['aprEnd'].value=localStorage.aprend;
document.forms['f1s1']['officerName'].value=localStorage.officerName;
document.forms['f1s1']['service'].value=localStorage.service;
document.forms['f1s1']['cadre'].value=localStorage.cadre;
document.forms['f1s1']['yearAllotment'].value=localStorage.yearAllot;
document.forms['f1s1']['dob'].value=localStorage.dob;
document.forms['f1s1']['presentGrade'].value=localStorage.pgrade;
document.forms['f1s1']['presentPost'].value=localStorage.ppost;
document.forms['f1s1']['dateAppoint'].value=localStorage.dateAppoint;
document.forms['f1s1']['reportingAuthName'].value=localStorage.reportingAuthName;
document.forms['f1s1']['reportingAuthPeriod'].value=localStorage.reportingAuthPeriod;
document.forms['f1s1']['reviewingAuthName'].value=localStorage.reviewAuthName;
document.forms['f1s1']['reviewingAuthPeriod'].value=localStorage.reviewAuthPeriod;
document.forms['f1s1']['acceptingAuthName'].value=localStorage.acceptingAuthName;
document.forms['f1s1']['acceptingAuthPeriod'].value=localStorage.acceptingAuthPerioid;
document.forms['f1s1']['leavePeriod'].value=localStorage.leavePeriod;
document.forms['f1s1']['leaveRemarks'].value=localStorage.leaveRemarks;
document.forms['f1s1']['leaveType'].value=localStorage.leaveType;
document.forms['f1s1']['otherLeavePeriod'].value=localStorage.otherLeavePeriod;
document.forms['f1s1']['otherLeaveType'].value=localStorage.otherLeaveType;
document.forms['f1s1']['otherLeaveRemarks'].value=localStorage.otherLeaveRemarks;

document.forms['f1s1']['trainingStart1'].value=localStorage.trainingStart1;
document.forms['f1s1']['trainingEnd1'].value=localStorage.trainingEnd1;
document.forms['f1s1']['trainingInstitute1'].value=localStorage.trainingInstitute1;
document.forms['f1s1']['trainingSubject1'].value=localStorage.trainingSubject1;
document.forms['f1s1']['trainingStart2'].value=localStorage.trainingStart2;
document.forms['f1s1']['trainingEnd2'].value=localStorage.trainingEnd2;
document.forms['f1s1']['trainingInstitute2'].value=localStorage.trainingInstitute2;
document.forms['f1s1']['trainingSubject2'].value=localStorage.trainingSubject2;
document.forms['f1s1']['award1'].value=localStorage.award1;
document.forms['f1s1']['award2'].value=localStorage.award2;
document.forms['f1s1']['detailPrevPar'].value=localStorage.detailPrevPar;
document.forms['f1s1']['propertyReturnDate'].value=localStorage.propertyReturnDate;
document.forms['f1s1']['medicalExamDate'].value=localStorage.medicalExamDate;
document.forms['f1s1']['dateSection1'].value=localStorage.dateSection1;	

//Read form 2 values
document.forms['f1s2']['desc'].value=localStorage.desc;

document.forms['f1s2']['task1DelInitial'].value=localStorage.task1DelInitial;
document.forms['f1s2']['task1DelMid'].value=localStorage.task1DelMid;
document.forms['f1s2']['task1DelAch'].value=localStorage.task1DelAch;
document.forms['f1s2']['task2DelInitial'].value=localStorage.task2DelInitial;
document.forms['f1s2']['task2DelMid'].value=localStorage.task2DelMid;
document.forms['f1s2']['task2DelAch'].value=localStorage.task2DelAch;
document.forms['f1s2']['task3DelInitial'].value=localStorage.task3DelInitial;
document.forms['f1s2']['task3DelMid'].value=localStorage.task3DelMid;
document.forms['f1s2']['task3DelAch'].value=localStorage.task3DelAch;
document.forms['f1s2']['task4DelInitial'].value=localStorage.task4DelInitial;
document.forms['f1s2']['task4DelMid'].value=localStorage.task4DelMid;
document.forms['f1s2']['task4DelAch'].value=localStorage.task4DelAch;
document.forms['f1s2']['exceptionalContri'].value=localStorage.exceptionalContri;
document.forms['f1s2']['factorsHinderedPerf'].value=localStorage.factorsHinderedPerf;
document.forms['f1s2']['trainingCurrent'].value=localStorage.trainingCurrent;
document.forms['f1s2']['trainingFuture'].value=localStorage.trainingFuture;
document.forms['f1s2']['dateSection2'].value=localStorage.dateSection2;
document.forms['f1s2']['propertyReturnDate'].value=localStorage.propertyReturnDate;
/**/
//Now read radio buttons
if(localStorage.propertyReturnDone=="true")
{
document.forms['f1s2']['propertyReturnDone'][0].checked=true;
}
else
{
document.forms['f1s2']['propertyReturnDone'][1].checked=true;
}

if(localStorage.medicalTestDone=="true")
{
	
	document.forms['f1s2']['medicalTestDone'][0].checked=true;
}
else
{
document.forms['f1s2']['medicalTestDone'][1].checked=true;
}	

if(localStorage.annualWorkPlanDone=="true")
{
	
	document.forms['f1s2']['annualWorkPlanDone'][0].checked=true;
}
else
{
document.forms['f1s2']['annualWorkPlanDone'][1].checked=true;
}	
}
else if(category==2)
{
	
//##################FORM 1 : SECTION 3 VALUES READ

document.forms['f1s3']['factualDetailsIfDisagree'].value=localStorage.factualDetailsIfDisagree;
document.forms['f1s3']['commentExceptionalClaim'].value=localStorage.commentExceptionalClaim;
document.forms['f1s3']['significantFailures'].value=localStorage.significantFailures;
document.forms['f1s3']['skillUpgradationComment'].value=localStorage.skillUpgrdationComment;
document.forms['f1s3']['commentIntegrity'].value=localStorage.commentIntegrity;
document.forms['f1s3']['commentWeakerSections'].value=localStorage.commentWeakerSections;
document.forms['f1s3']['overallGradeSec3'].value=localStorage.overallGrade;
document.forms['f1s3']['dateSection3'].value=localStorage.dateSection3;
document.forms['f1s3']['accomplishmentRep'].value=localStorage.accomplishmentRep;
document.forms['f1s3']['accomplishmentRev'].value=localStorage.accomplishmentRev;

//###################FORM 1 : Section 4,5 Read Values
}
else if(category==3)
{

if(localStorage.assessmentAgreeTill3=="true")
{
	
	document.forms['f1s4']['assessmentAgreeTill3'][0].checked;
	}
else
{
document.forms['f1s4']['assessmentAgreeTill3'][1].checked;
}	
document.forms['f1s4']['differenceOpinion4'].value=localStorage.differenceOpinion4;
document.forms['f1s4']['commentsPenPictures'].value=localStorage.commentsPenPicture;
document.forms['f1s4']['overallGradeSec4'].value=localStorage.overallGradeSec4;
document.forms['f1s4']['dateSection4'].value=localStorage.dateSection4;
}
else if(category==4)
{
if(localStorage.assessmentAgreeTill4=="true")
{
	
	document.forms['f1s5']['assessmentAgreeTill4'][0].checked;
	}
else
{
document.forms['f1s5']['assessmentAgreeTill4'][1].checked;
}	


document.forms['f1s5']['differenceOpinion5'].value=localStorage.differenceOpinion5;
document.forms['f1s5']['overallGradeSec5'].value=localStorage.overallGradeSec5;
document.forms['f1s5']['dateSection5'].value=localStorage.dateSection5;
/**/
}
}
}