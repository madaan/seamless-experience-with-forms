function saveLocalData(category)
{
//sg
//check if HTML5 is supported by the browser
if(typeof(Storage)!=="undefined")
{
localStorage.saved=1;
if(category==1)
{
localStorage.aprstart=document.forms['f1s1']['aprStart'].value;
localStorage.aprend=document.forms['f1s1']['aprEnd'].value;
localStorage.officerName=document.forms['f1s1']['officerName'].value;
localStorage.service=document.forms['f1s1']['service'].value;
localStorage.cadre=document.forms['f1s1']['cadre'].value;
localStorage.yearAllot=document.forms['f1s1']['yearAllotment'].value;
localStorage.dob=document.forms['f1s1']['dob'].value;
if(document.forms['f1s1']['presentGrade'].value!=="undefined")
localStorage.pgrade=document.forms['f1s1']['presentGrade'].value;
localStorage.ppost=document.forms['f1s1']['presentPost'].value;

localStorage.dateAppoint=document.forms['f1s1']['dateAppoint'].value;

localStorage.reportingAuthName=document.forms['f1s1']['reportingAuthName'].value;

localStorage.reportingAuthPeriod=document.forms['f1s1']['reportingAuthPeriod'].value;

localStorage.reviewAuthName=document.forms['f1s1']['reviewingAuthName'].value;

localStorage.reviewAuthPeriod=document.forms['f1s1']['reviewingAuthPeriod'].value;

localStorage.acceptingAuthName=document.forms['f1s1']['acceptingAuthName'].value;
localStorage.acceptingAuthPerioid=document.forms['f1s1']['acceptingAuthPeriod'].value;
localStorage.leavePeriod=document.forms['f1s1']['leavePeriod'].value;

localStorage.leaveRemarks=document.forms['f1s1']['leaveRemarks'].value;
localStorage.leaveType=document.forms['f1s1']['leaveType'].value;
localStorage.otherLeavePeriod=document.forms['f1s1']['otherLeavePeriod'].value;
localStorage.otherLeaveType=document.forms['f1s1']['otherLeaveType'].value;
localStorage.otherLeaveRemarks=document.forms['f1s1']['otherLeaveRemarks'].value;

localStorage.trainingStart1=document.forms['f1s1']['trainingStart1'].value;
localStorage.trainingEnd1=document.forms['f1s1']['trainingEnd1'].value;
localStorage.trainingInstitute1=document.forms['f1s1']['trainingInstitute1'].value;
localStorage.trainingSubject1=document.forms['f1s1']['trainingSubject1'].value;

localStorage.trainingStart2=document.forms['f1s1']['trainingStart2'].value;
localStorage.trainingEnd2=document.forms['f1s1']['trainingEnd2'].value;
localStorage.trainingInstitute2=document.forms['f1s1']['trainingInstitute2'].value;
localStorage.trainingSubject2=document.forms['f1s1']['trainingSubject2'].value;

localStorage.award1=document.forms['f1s1']['award1'].value;
localStorage.award2=document.forms['f1s1']['award2'].value;
localStorage.detailPrevPar=document.forms['f1s1']['detailPrevPar'].value;
localStorage.propertyReturnDate=document.forms['f1s1']['propertyReturnDate'].value;
localStorage.medicalExamDate=document.forms['f1s1']['medicalExamDate'].value;

localStorage.dateSection1=document.forms['f1s1']['dateSection1'].value;

//Form 1 Section 2 Local storage definition begins

localStorage.desc=document.forms['f1s2']['desc'].value;

localStorage.task1DelInitial=document.forms['f1s2']['task1DelInitial'].value;
localStorage.task1DelMid=document.forms['f1s2']['task1DelMid'].value;
localStorage.task1DelAch=document.forms['f1s2']['task1DelAch'].value;
localStorage.task2DelInitial=document.forms['f1s2']['task2DelInitial'].value;
localStorage.task2DelMid=document.forms['f1s2']['task2DelMid'].value;
localStorage.task2DelAch=document.forms['f1s2']['task2DelAch'].value;
localStorage.task3DelInitial=document.forms['f1s2']['task3DelInitial'].value;
localStorage.task3DelMid=document.forms['f1s2']['task3DelMid'].value;
localStorage.task3DelAch=document.forms['f1s2']['task3DelAch'].value;
localStorage.task4DelInitial=document.forms['f1s2']['task4DelInitial'].value;
localStorage.task4DelMid=document.forms['f1s2']['task4DelMid'].value;
localStorage.task4DelAch=document.forms['f1s2']['task4DelAch'].value;
localStorage.propertyReturnDate=document.forms['f1s2']['propertyReturnDate'].value;
localStorage.exceptionalContri=document.forms['f1s2']['exceptionalContri'].value;
localStorage.factorsHinderedPerf=document.forms['f1s2']['factorsHinderedPerf'].value;
localStorage.trainingCurrent=document.forms['f1s2']['trainingCurrent'].value;
localStorage.trainingFuture=document.forms['f1s2']['trainingFuture'].value;
localStorage.dateSection2=document.forms['f1s2']['dateSection2'].value;
/**/
/* Checking the radio buttons */

if(document.forms['f1s2']['propertyReturnDone'][0].checked)
{
	localStorage.propertyReturnDone=true;
}
else
{
	localStorage.propertyReturnDone=false;

}	
if(document.forms['f1s2']['medicalTestDone'][0].checked)
{
	alert('Alright!');
	localStorage.medicalTestDone=true;
}
else
{
	localStorage.medicalTestDone=false;

}	
if(document.forms['f1s2']['annualWorkPlanDone'][0].checked)
{
	localStorage.annualWorkPlanDone=true;
}
else
{
	localStorage.annualWorkPlanDone=false;

}	
}
else if(category==2) //review officer
{

//##############################FORM1 : SECTION 3 ####################################3

//localStorage.=document.forms['f1s3'][''].value;
localStorage.factualDetailsIfDisagree=document.forms['f1s3']['factualDetailsIfDisagree'].value;
localStorage.commentExceptionalClaim=document.forms['f1s3']['commentExceptionalClaim'].value;
localStorage.significantFailures=document.forms['f1s3']['significantFailures'].value;
localStorage.skillUpgrdationComment=document.forms['f1s3']['skillUpgradationComment'].value;
localStorage.commentIntegrity=document.forms['f1s3']['commentIntegrity'].value;
localStorage.commentWeakerSections=document.forms['f1s3']['commentWeakerSections'].value;
localStorage.overallGradeSec3=document.forms['f1s3']['overallGradeSec3'].value;
localStorage.dateSection3=document.forms['f1s3']['dateSection3'].value;
localStorage.accomplishmentRep=document.forms['f1s3']['accomplishmentRep'].value;
localStorage.accomplishmentRev=document.forms['f1s3']['accomplishmentRev'].value;
}
else if(category==3) //reporting officer
{
//##############################FORM1 : SECTION 4 ############
//localStorage.=document.forms['f1s4'][''].value;
if(document.forms['f1s4']['assessmentAgreeTill3'][0].checked)
{
	localStorage.assessmentAgreeTill3=true;
}
else
{
	localStorage.assessmentAgreeTill3=false;
}	
localStorage.differenceOpinion4=document.forms['f1s4']['differenceOpinion4'].value;
localStorage.commentsPenPicture=document.forms['f1s4']['commentsPenPictures'].value;
localStorage.overallGradeSec4=document.forms['f1s4']['overallGradeSec4'].value;
localStorage.dateSection4=document.forms['f1s4']['dateSection4'].value;
}
else if(category==4) //accepting authority
{

//################################ FORM 1 : Section 5 ###########
//localStorage.=document.forms['f1s5'][''].value;

if(document.forms['f1s5']['assessmentAgreeTill4'][0].checked)
{
	localStorage.assessmentAgreeTill4=true;
}
else
{
	localStorage.assessmentAgreeTill4=false;
}	
localStorage.differenceOpinion5=document.forms['f1s5']['differenceOpinion5'].value;
localStorage.overallGradeSec5=document.forms['f1s5']['overallGradeSec5'].value;
localStorage.dateSection5=document.forms['f1s5']['dateSection5'].value;

}
/**/
alert('Values Have Been Saved.\nYou may continue filling this form later');
}
       else
         {
alert('Your Browser Does Not Support HTML5\n Consider Upgrading');

         }
}